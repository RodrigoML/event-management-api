# Event Management API

## Descripción

La Event Management API es un servicio REST desarrollado con Spring Boot. Este servicio permite realizar operaciones CRUD (Crear, Leer, Actualizar, Eliminar) sobre eventos. Cada evento se caracteriza por propiedades como identificador único, nombre, descripción, fecha y ubicación.

## Documentación
La documentación de la API se encuentra disponible en el archivo `Swagger-Documentation.yaml`. Puedes acceder a ella [aquí](Swagger-Documentation.yaml).

## Repositorio
El repositorio de la API se encuentra disponible en [GitLab](https://gitlab.com/RodrigoML/event-management-api.git)
## Funcionalidades

- **Creación de Eventos:** Permite añadir nuevos eventos al sistema.
- **Consulta de Eventos:** Se pueden obtener los detalles de eventos específicos o listar todos los eventos disponibles.
- **Actualización de Eventos:** Facilita la modificación de la información de eventos existentes.
- **Eliminación de Eventos:** Permite eliminar eventos del sistema.

## Requisitos Previos

Para ejecutar y desarrollar la Event Management API, se requiere:

- JDK 11 o superior.
- Maven 3.6 o superior.
- MySQL 8.0 o superior.

## Configuración de la Base de Datos

1. Iniciar el servidor MySQL.
2. Crear una base de datos con el nombre `event_management_api`.
3. Ejecutar el script SQL proporcionado en `src/main/resources/db/` para crear la estructura necesaria en la base de datos.

## Ejecución de la API

Sigue estos pasos para ejecutar la API:

1. Abrir una terminal.
2. Navegar al directorio raíz del proyecto.
3. Ejecutar:

```bash
mvn spring-boot:run
```
## Desarrollo

El desarrollo de la Event Management API se centró en seguir buenas prácticas y patrones de diseño para asegurar una arquitectura mantenible.

- **Spring Boot:** Utilizado para simplificar la configuración y el despliegue de la aplicación.
- **Modelo-Vista-Controlador (MVC):** Patrón arquitectónico implementado para separar la lógica de negocio, la interfaz de usuario y el control de la entrada.
- **Principios SOLID:** Adoptados para promover un diseño de software más comprensible, flexible y mantenible.
- **Inyección de Dependencias:** Utilizada para desacoplar la construcción de objetos y mejorar la testabilidad.
- **Spring Data JPA:** Facilita la interacción con la base de datos a través de repositorios, minimizando el código necesario para el acceso a datos.
- **Lombok:** Empleado para reducir el código repetitivo mediante la generación automática de getters, setters, constructores, entre otros.
- **DTO (Data Transfer Object):** Patrón utilizado para transferir datos entre sub-sistemas de la aplicación, simplificando los objetos de dominio para las respuestas de la API.
- **Manejo Global de Excepciones:** Implementado con `@ControllerAdvice` para gestionar las excepciones de forma centralizada y proporcionar respuestas consistentes.


## Pruebas de la API con Postman
### Prueba de obtener todos los eventos:
![postman-create-event.png.png](src%2Fmain%2Fresources%2Fimages%2Fpostman-create-event.png.png)
### Prueba de obtener un evento por ID:
![postman-get-event-by-id.png](src%2Fmain%2Fresources%2Fimages%2Fpostman-get-event-by-id.png)
### Prueba de crear un evento:
![postman-get-all-events.png](src%2Fmain%2Fresources%2Fimages%2Fpostman-create-event.png)
### Prueba de actualizar un evento:
![postman-update-event.png](src%2Fmain%2Fresources%2Fimages%2Fpostman-update-event.png)
### Prueba de eliminar un evento:
![postman-delete-event.png](src%2Fmain%2Fresources%2Fimages%2Fpostman-delete-event.png)

## Pruebas Unitarias
Se han implementado pruebas unitarias para validar el comportamiento de la API. Estas pruebas se encuentran en el directorio `src/test/java`.
Las clases que se han probado son:
- `EventControllerTest`

![img.png](src/main/resources/images/img.png)

- `EventServiceImplTest`

![img_1.png](src/main/resources/images/img_1.png)

- `EventMapperTest`

![img_2.png](src%2Fmain%2Fresources%2Fimages%2Fimg_2.png)

## Imagenes de la Documentación de Swagger
### Swagger UI
![swagger-ui.png](src%2Fmain%2Fresources%2Fimages%2Fswagger-ui.png)
![swagger-get.png](src%2Fmain%2Fresources%2Fimages%2Fswagger-get.png)
![swagger-post.png](src%2Fmain%2Fresources%2Fimages%2Fswagger-post.png)
![swagger-get-id.png](src%2Fmain%2Fresources%2Fimages%2Fswagger-get-id.png)
![swagger-put.png](src%2Fmain%2Fresources%2Fimages%2Fswagger-put.png)
![swagger-delete.png](src%2Fmain%2Fresources%2Fimages%2Fswagger-delete.png)
![swagger-schemes.png](src%2Fmain%2Fresources%2Fimages%2Fswagger-schemes.png)