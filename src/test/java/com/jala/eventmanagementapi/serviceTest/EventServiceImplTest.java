package com.jala.eventmanagementapi.serviceTest;

import com.jala.eventmanagementapi.dto.EventDTO;
import com.jala.eventmanagementapi.mapper.EventMapper;
import com.jala.eventmanagementapi.model.Event;
import com.jala.eventmanagementapi.repository.EventRepository;
import com.jala.eventmanagementapi.service.impl.EventServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class EventServiceImplTest {

    @InjectMocks
    private EventServiceImpl eventService;

    @Mock
    private EventRepository eventRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void createEventSuccess() {
        Event mockEvent = new Event();
        EventDTO mockEventDTO = EventMapper.toDTO(mockEvent);
        when(eventRepository.save(any(Event.class))).thenReturn(mockEvent);

        EventDTO result = eventService.createEvent(mockEventDTO);

        assertNotNull(result);
        verify(eventRepository).save(any(Event.class));
    }

    @Test
    void getEventByIdSuccess() {
        Event mockEvent = new Event();
        Long eventId = 1L;
        when(eventRepository.findById(eq(eventId))).thenReturn(Optional.of(mockEvent));

        EventDTO result = eventService.getEventById(eventId);

        assertNotNull(result);
        verify(eventRepository).findById(eq(eventId));
    }

    @Test
    void getEventByIdNotFound() {
        Long eventId = 1L;
        when(eventRepository.findById(eq(eventId))).thenReturn(Optional.empty());

        assertThrows(RuntimeException.class, () -> eventService.getEventById(eventId));
        verify(eventRepository).findById(eq(eventId));
    }

    @Test
    void getAllEventsSuccess() {
        List<Event> events = Arrays.asList(new Event(), new Event());
        when(eventRepository.findAll()).thenReturn(events);

        List<EventDTO> result = eventService.getAllEvents();

        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(events.size(), result.size());
        verify(eventRepository).findAll();
    }

    @Test
    void updateEventSuccess() {
        Event existingEvent = new Event();
        existingEvent.setId(1L);
        EventDTO eventDTO = new EventDTO();
        eventDTO.setId(1L);
        when(eventRepository.findById(eq(1L))).thenReturn(Optional.of(existingEvent));
        when(eventRepository.save(any(Event.class))).thenReturn(existingEvent);

        EventDTO result = eventService.updateEvent(1L, eventDTO);

        assertNotNull(result);
        verify(eventRepository).findById(eq(1L));
        verify(eventRepository).save(any(Event.class));
    }

    @Test
    void updateEventNotFound() {
        Long eventId = 1L;
        EventDTO eventDTO = new EventDTO();
        when(eventRepository.findById(eq(eventId))).thenReturn(Optional.empty());

        assertThrows(RuntimeException.class, () -> eventService.updateEvent(eventId, eventDTO));
        verify(eventRepository).findById(eq(eventId));
    }

    @Test
    void deleteEventSuccess() {
        Long eventId = 1L;
        doNothing().when(eventRepository).deleteById(eq(eventId));
        when(eventRepository.existsById(eq(eventId))).thenReturn(true);

        eventService.deleteEvent(eventId);

        verify(eventRepository).deleteById(eq(eventId));
    }

    @Test
    void deleteEventNotFound() {
        Long eventId = 1L;
        when(eventRepository.existsById(eq(eventId))).thenReturn(false);

        assertThrows(RuntimeException.class, () -> eventService.deleteEvent(eventId));
        verify(eventRepository, never()).deleteById(eq(eventId));
    }
}