package com.jala.eventmanagementapi.mapperTest;


import com.jala.eventmanagementapi.dto.EventDTO;
import com.jala.eventmanagementapi.mapper.EventMapper;
import com.jala.eventmanagementapi.model.Event;
import org.junit.jupiter.api.Test;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class EventMapperTest {

    @Test
    public void whenConvertEventEntityToEventDtoThenCorrect() {
        Event event = new Event();
        event.setId(1L);
        event.setName("Test Event");
        event.setDescription("This is a test event");
        event.setDate(LocalDate.of(2024, 4, 4));
        event.setLocation("Test Location");

        EventDTO eventDTO = EventMapper.toDTO(event);

        assertNotNull(eventDTO);
        assertEquals(event.getId(), eventDTO.getId());
        assertEquals(event.getName(), eventDTO.getName());
        assertEquals(event.getDescription(), eventDTO.getDescription());
        assertEquals(event.getDate(), eventDTO.getDate());
        assertEquals(event.getLocation(), eventDTO.getLocation());
    }

    @Test
    public void whenConvertEventDtoToEventEntityThenCorrect() {
        EventDTO eventDTO = new EventDTO();
        eventDTO.setId(2L);
        eventDTO.setName("Another Test Event");
        eventDTO.setDescription("This is another test event");
        eventDTO.setDate(LocalDate.of(2024, 5, 5));
        eventDTO.setLocation("Another Test Location");

        Event event = EventMapper.toEntity(eventDTO);

        assertNotNull(event);
        assertEquals(eventDTO.getId(), event.getId());
        assertEquals(eventDTO.getName(), event.getName());
        assertEquals(eventDTO.getDescription(), event.getDescription());
        assertEquals(eventDTO.getDate(), event.getDate());
        assertEquals(eventDTO.getLocation(), event.getLocation());
    }

    @Test
    public void whenConvertNullEventEntityThenNullEventDto() {
        EventDTO eventDTO = EventMapper.toDTO(null);

        assertNull(eventDTO);
    }

    @Test
    public void whenConvertNullEventDtoThenNullEventEntity() {
        Event event = EventMapper.toEntity(null);

        assertNull(event);
    }
}
