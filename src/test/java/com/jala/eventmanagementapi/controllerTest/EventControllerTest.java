package com.jala.eventmanagementapi.controllerTest;

import com.jala.eventmanagementapi.controller.EventController;
import com.jala.eventmanagementapi.dto.EventDTO;
import com.jala.eventmanagementapi.service.EventService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class EventControllerTest {
    @InjectMocks
    private EventController eventController;

    @Mock
    private EventService eventService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void createEventSuccess() {
        EventDTO mockEventDTO = new EventDTO();
        when(eventService.createEvent(any(EventDTO.class))).thenReturn(mockEventDTO);

        ResponseEntity<EventDTO> response = eventController.createEvent(mockEventDTO);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        verify(eventService).createEvent(any(EventDTO.class));
    }

    @Test
    void getEventByIdSuccess() {
        EventDTO mockEventDTO = new EventDTO();
        Long eventId = 1L;
        when(eventService.getEventById(eq(eventId))).thenReturn(mockEventDTO);

        ResponseEntity<EventDTO> response = eventController.getEventById(eventId);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(eventService).getEventById(eq(eventId));
    }

    @Test
    void getAllEventsSuccess() {
        List<EventDTO> mockEventDTOs = Arrays.asList(new EventDTO(), new EventDTO());
        when(eventService.getAllEvents()).thenReturn(mockEventDTOs);

        ResponseEntity<List<EventDTO>> response = eventController.getAllEvents();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(eventService).getAllEvents();
    }

    @Test
    void updateEventSuccess() {
        EventDTO mockEventDTO = new EventDTO();
        Long eventId = 1L;
        when(eventService.updateEvent(eq(eventId), any(EventDTO.class))).thenReturn(mockEventDTO);

        ResponseEntity<EventDTO> response = eventController.updateEvent(eventId, mockEventDTO);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(eventService).updateEvent(eq(eventId), any(EventDTO.class));
    }

    @Test
    void deleteEventSuccess() {
        Long eventId = 1L;
        doNothing().when(eventService).deleteEvent(eq(eventId));

        ResponseEntity<Void> response = eventController.deleteEvent(eventId);

        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(eventService).deleteEvent(eq(eventId));
    }
}