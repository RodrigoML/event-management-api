CREATE DATABASE IF NOT EXISTS event_management_api;
USE event_management_api;

CREATE TABLE events (
                        id BIGINT AUTO_INCREMENT PRIMARY KEY,
                        name VARCHAR(255) NOT NULL,
                        description TEXT NOT NULL,
                        date DATE NOT NULL,
                        location VARCHAR(255) NOT NULL
);
