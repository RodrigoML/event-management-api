INSERT INTO events (name, description, date, location) VALUES
                                                           ('Conferencia sobre APIs', 'Una conferencia para discutir las últimas tendencias en APIs.', '2024-04-20', 'Centro de Convenciones Ciudad'),
                                                           ('Taller de cocina vegana', 'Aprende a cocinar deliciosos platos veganos con un chef experto.', '2024-05-15', 'Cocina Creativa Espacio'),
                                                           ('Maratón de la ciudad', 'Participa en el maratón anual y corre por una buena causa.', '2024-09-10', 'Parque Central'),
                                                           ('Exposición de arte moderno', 'Explora las últimas tendencias del arte moderno.', '2024-07-03', 'Museo de Arte Contemporáneo'),
                                                           ('Festival de música electrónica', 'Disfruta de los mejores DJs en este festival al aire libre.', '2024-08-21', 'Isla Música'),
                                                           ('Curso de programación para principiantes', 'Iníciate en el mundo de la programación con este curso intensivo.', '2024-06-05', 'Academia de Código'),
                                                           ('Torneo de ajedrez', 'Demuestra tus habilidades en el ajedrez en nuestro torneo anual.', '2024-10-17', 'Club de Ajedrez Rey'),
                                                           ('Feria tecnológica', 'Descubre las últimas innovaciones en tecnología y gadgets.', '2024-11-28', 'Pabellón Tech'),
                                                           ('Retiro de yoga y meditación', 'Recarga energías con este retiro enfocado en el bienestar y la paz interior.', '2024-12-05', 'Centro Holístico Luz'),
                                                           ('Cata de vinos', 'Explora una selección de los mejores vinos con expertos sommeliers.', '2024-03-18', 'Vinoteca Los Robles');
